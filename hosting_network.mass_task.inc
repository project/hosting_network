<?php

// TODO: If you run a migrate without having the feature enabled, it WILL break
// the sites.

function hosting_network_get_mass_tasks() {
  $raw_tasks = module_invoke_all('hosting_network_mass_tasks');

  $tasks = array();

  // Set defaults (you can't modify a foreach array item).
  foreach ($raw_tasks as $task_type => $task) {
    if (empty($task['subform'])) {
      $task['subform'] = 'hosting_network_mass_task_basic_subform';
    }
    if (empty($task['confirm_subform'])) {
      $task['confirm_subform'] = 'hosting_network_basic_confirm_subform';
    }

    $tasks[$task_type] = $task;
  }

  return $tasks;
}

/**
 * Implements hook_hosting_network_mass_tasks()
 *
 * We can just assume that mandatory features (hosting_site etc) are enabled.
 */
function hosting_network_hosting_network_mass_tasks() {
  $tasks = array(
    'migrate' => array(
      'label' => 'Migrate platforms',
      'subform' => 'hosting_network_mass_migrate_subform',
      'confirm_subform' => 'hosting_network_confirm_migrate_subform',
      'requires' => 'hosting_migrate',
    ),
    'verify' => array(
      'label' => 'Verify sites',
    ),
    'backup' => array(
      'label' => 'Backup sites',
    ),
    'enable' => array(
      'label' => 'Enable sites',
    ),
    'disable' => array(
      'label' => 'Disable sites',
    ),
    'delete' => array(
      'label' => 'Delete sites',
    ),
  );

  return $tasks;
}

function hosting_network_mass_task_form($form, &$form_state, $task_type) {
  $form['run_message'] = array(
    '#markup' => t('Run !task_type accross multiple servers.', array('!task_type' => $task_type)),
  );
  
  $form['task_type'] = array('#type' => 'hidden', '#value' => $task_type);

  $network_nodes = hosting_get_servers('network_node');

  $servers = array();
  foreach($network_nodes as $nid => $hostname) {
    $node = node_load($nid);
    $servers[$nid] = $hostname;
  }

  $form['servers'] = array(
    '#type' => 'checkboxes',
    '#options' => $servers,
    '#title' => t('Servers'),
    '#required' => TRUE,
  );

  $mass_tasks = hosting_network_get_mass_tasks();
  if (!empty($mass_tasks[$task_type])) {
    $form = call_user_func($mass_tasks[$task_type]['subform'], $form, $form_state);
  }
  else {
    drupal_set_message(t('Task type !task_type is not implemented for mass tasks.', array('!task_type' => $task_type)), 'error');
  }

  $form['submit'] = array('#type' => 'submit', '#value' => t('Next'));

  return $form;
}

function hosting_network_mass_task_basic_subform($form, &$form_state) {
  $task_type = $form['task_type']['#value'];

  $form['site_pattern'] = array(
    '#type' => 'textfield',
    '#title' => t('Restrict to sites matching this pattern'),
    '#description' => t('Leave this blank to run on all sites, or enter a regular expression (eg /test/).'),
    '#size' => 60,
    '#maxlength' => 256,
    '#required' => FALSE,
  );

  $form['#validate'][] = 'hosting_network_mass_basic_subform_validate';
  $form['#submit'][] = 'hosting_network_mass_basic_subform_submit';

  return $form;
}

function hosting_network_mass_basic_subform_submit(&$form, &$form_state) {
  global $user;

  $servers = array();

  foreach ($form_state['values']['servers'] as $nid => $selected) {
    if ($selected) {
      $servers[] = $selected;
    }
  }

  $options['query'] = array(
    'site_pattern' => $form_state['values']['site_pattern'],
    'servers' => $servers,
    'token' => drupal_get_token($user->uid),
  );

  $path = 'hosting_network/mass_task/' . $form_state['values']['task_type'] . '/confirm';

  drupal_goto($path, $options);
}

function hosting_network_mass_migrate_subform($form, &$form_state) {
  $form['source_platform'] = array(
    '#type' => 'textfield',
    '#title' => t('Source platform'),
    '#description' => t('The platform where the sites currently are. Use forward slashes to indicate a regex (/search/).'),
    '#size' => 60,
    '#maxlength' => 256,
    '#required' => TRUE,
  );

  $form['destination_platform'] = array(
    '#type' => 'textfield',
    '#title' => t('Destination platform'),
    '#description' => t('The platform you want to migrate the sites to. Use forward slashes to indicate a regex (/search/).'),
    '#size' => 60,
    '#maxlength' => 256,
    '#required' => TRUE,
  );

  $form['site_pattern'] = array(
    '#type' => 'textfield',
    '#title' => t('Restrict to sites matching this pattern'),
    '#description' => t('Leave this blank to run on all sites on the platform, or enter /pattern/'),
    '#size' => 60,
    '#maxlength' => 256,
    '#required' => FALSE,
  );

  $form['#validate'][] = 'hosting_network_mass_basic_subform_validate';
  $form['#submit'][] = 'hosting_network_mass_migrate_subform_submit';

  return $form;
}

function hosting_network_mass_basic_subform_validate(&$form, &$form_state) {
  $site_pattern = $form_state['values']['site_pattern'];

  if (!empty($site_pattern) && ('/' !== substr($site_pattern, 0, 1))) {
    form_set_error('site_pattern', t('The site pattern pattern must be a regex. In most cases you search for a simple string with this pattern: /searchstring/'));
  }
}

function hosting_network_basic_confirm_subform($form, $form_state) {
  $server_nids = array_values($_GET['servers']);
  $site_pattern = $_GET['site_pattern'];
  $task_type = $form['#task_type'];

  $tasks = hosting_network_compute_basic_tasks($server_nids, $task_type, $site_pattern);
  $form['#tasks'] = $tasks;

  $form['tasks_markup'] = array(
    '#markup' => theme('hosting_network_mass_confirm_basic_task_list', array('tasks' => $tasks)),
  );

  $form['#submit'][] = 'hosting_network_mass_basic_confirm_submit';

  return $form;
}

function hosting_network_mass_basic_confirm_submit(&$form, &$form_state) {
  $tasks = $form['#tasks'];

  foreach ($tasks as $server_nid => $server_tasks) {
    $server = node_load($server_nid);
    drupal_set_message(t("Server !server_nid (!server_name): ", array('!server_name' => $server->title, '!server_nid' => $server_nid)));

    foreach ($server_tasks as $index => $task) {
      // Pay no attention to the non-number "tasks" that are just metadata
      if (is_numeric($index)) {
        $task_array = array(
          'type' => $task['task_type'],
          'nid' => $task['target_nid'],
        );

        $result = hosting_network_rest_call($server_nid, 'hosting_task', $task_array, 'POST', $debug);

        if (!empty($result)) {
          $msg_array = array(
            '!site_name' => $task['site_name'],
            '!task_type' => $task['task_type'],
          );

          drupal_set_message(t('Running !task_type on !site_name', $msg_array));
        } else {
          drupal_set_message(t('Could not run !task_type on !site_name', $msg_array), 'error');
        }
      }
    }

    drupal_set_message(t("Total tasks for server !server_nid: ", array('!server_nid' => $server_nid)) . (count($server_tasks)));
  }

  drupal_goto('hosting_network/nodes');
}

function hosting_network_mass_migrate_subform_submit(&$form, &$form_state) {
  global $user;

  $servers = array();

  foreach ($form_state['values']['servers'] as $nid => $selected) {
    if ($selected) {
      $servers[] = $selected;
    }
  }

  $source_platform = $form_state['values']['source_platform'];
  $destination_platform = $form_state['values']['destination_platform'];

  $options['query'] = array(
    'source_platform' => $form_state['values']['source_platform'],
    'destination_platform' => $form_state['values']['destination_platform'],
    'site_pattern' => $form_state['values']['site_pattern'],
    'servers' => $servers,
    'token' => drupal_get_token($user->uid),
  );

  $path = 'hosting_network/mass_task/' . $form_state['values']['task_type'] . '/confirm';

  drupal_goto($path, $options);
}

function hosting_network_confirm_task_form($form, &$form_state, $task_type) {
  // Can't end the hook with a task name since it might call other hooks such as
  // cron in an unpredictable manner (depending on the name of the task).
  $mass_tasks = hosting_network_get_mass_tasks();

  $form['#task_type'] = $task_type;
  if (!empty($mass_tasks[$task_type])) {
    $form = call_user_func($mass_tasks[$task_type]['confirm_subform'], $form, $form_state);
  }

  if (empty($form)) {
    drupal_set_message(t('Task type !task_type is not implemented for mass tasks.', array('!task_type' => $task_type)), 'error');
  }

  $form['task_type_markup'] = array(
    '#markup' => '<p>' . t('Confirm mass !task_type', array('!task_type' => $task_type)) . '</p>',
    '#weight' => -1000,
  );

  $form['confirmation_intro'] = array(
    '#markup' => t('These operations will be run on each server:'),
    '#weight' => -999,
  );

  $question = t("Aegir Network: confirm mass @task?", array('@task' => $task_type));
  $path = !empty($_REQUEST['destination'])? $_REQUEST['destination']: 'hosting_network/nodes';
  $form = confirm_form($form, $question, $path, '', t('Confirm'));

  return $form;
}

function hosting_network_confirm_migrate_subform($form, $form_state) {
  $source_platform = $_GET['source_platform'];
  $destination_platform = $_GET['destination_platform'];
  $server_nids = array_values($_GET['servers']);
  $site_pattern = $_GET['site_pattern'];

  if (empty($source_platform)) {
    // Should never happen in a normal workflow.
    throw new Exception("Missing argument: Source platform.");
  }

  if (empty($destination_platform)) {
    // Should never happen in a normal workflow.
    throw new Exception("Missing argument: Destination platform.");
  }

  $tasks = hosting_network_compute_mass_migration_tasks($server_nids, $source_platform, $destination_platform, $site_pattern);
  $form['#tasks'] = $tasks;

  $form['tasks_markup'] = array(
    '#markup' => theme('hosting_network_mass_migration_list', array('tasks' => $tasks)),
  );

  $form['#submit'][] = 'hosting_network_mass_migrate_confirm_submit';

  return $form;
}

function hosting_network_mass_migrate_confirm_submit(&$form, &$form_state) {
  $tasks = $form['#tasks'];

  foreach ($tasks as $server_nid => $server_tasks) {
    $platform = $server_tasks['platform']['human_readable'];
    drupal_set_message(t("Server !server_nid (!server_name): ", array('!server_name' => $platform['server'], '!server_nid' => $server_nid)));

    foreach ($server_tasks as $index => $task) {
      // Pay no attention to the non-number "tasks" that are just metadata
      if (is_numeric($index)) {
        $task_array = array(
          'type' => 'migrate',
          'nid' => $task['target_nid'],
          'options' => $task['options'],
        );

        $result = hosting_network_rest_call($server_nid, 'hosting_task', $task_array, 'POST', $debug);

        if (!empty($result)) {
          $msg_array = array(
            '!site_name' => $task['site_name'],
            '!source_platform' => $platform['source'],
            '!destination_platform' => $platform['destination'],
          );

          drupal_set_message(t('Migrating !site_name from !source_platform to !destination_platform.', $msg_array));
        } else {
          drupal_set_message(t('Could not migrate !site_name. Check server settings.', array('!site_name' => $task['site_name'])), 'error');
        }
      }
    }

    // Substract one for the "task" that is simply metadata
    drupal_set_message(t("Total tasks for server !server_nid: ", array('!server_nid' => $server_nid)) . (count($server_tasks) - 1));
  }

  drupal_goto('hosting_network/nodes');
}

/**
 * Finds which platforms to run the mass tasks on for a specific server
 *
 * @param $server_nids
 *   Array of server nids, representing all the servers we want to run these on
 * @param $source_str string
 *   The source platform, where the existing sites reside. If this starts and
 *   ends with a forward slash (/), it will be assumed to be a regex.
 * @param $destination_str string
 *   The destination platform, where we want to move the sites to. If this
 *   starts with a forward slash (/), it will be assumed to be a regex.
 * @param $site_pattern string
 *   regex to restrict which sites to run the task on. Compares to site->title
 * @return An associative array of tasks that must be run per server (indexed by
 *   server nid). Includes a human-readable array and the computed values array.
 */
function hosting_network_compute_mass_migration_tasks($server_nids, $source_str, $destination_str, $site_pattern = '') {
  $servers = node_load_multiple(array_values($_GET['servers']));
  $tasks = array();

  $source_is_regex = ('/' == substr($source_str, 0, 1));
  $destination_is_regex = ('/' == substr($destination_str, 0, 1));

  foreach ($server_nids as $server_nid) {
    $platforms = hosting_network_get_platform_list($server_nid);

    $source_platform = NULL;
    $destination_platform = NULL;
    // Note: For some reason the array isn't associative.
    foreach ($platforms as $platform) {
      if ($source_is_regex) {
        if (preg_match($source_str, $platform->title)) {
          $source_platform = $platform;
        }
      }
      else {
        if ($platform->title == $source_str) {
          $source_platform = $platform;
        }
      }

      if ($destination_is_regex) {
        if (preg_match($destination_str, $platform->title)) {
          $destination_platform = $platform;
        }
      }
      else {
        if ($platform->title == $destination_str) {
          $destination_platform = $platform;
        }
      }

      if (!empty($source_platform) && !empty($destination_platform)) {
        // The platform task is not a "real" task in that it doesn't get run.
        $tasks[$server_nid]['platform'] = array(
          'human_readable' => array(
            'server' => $servers[$server_nid]->title,
            'source' => $source_platform->title,
            'destination' => $destination_platform->title,
          ),
        );

        $sites = (array) hosting_network_rest_call($server_nid, 'hosting_platform/sites', array('nid' => $source_platform->nid), 'POST');

        foreach ($sites as $site) {
          if (empty($site_pattern) || preg_match($site_pattern, $site->title)) {
            $tasks[$server_nid][] = array(
              'site_name' => $site->title,
              'server_nid' => $server_nid,
              'target_nid' => $site->nid,
              'options' => array(
                'target_platform' => $destination_platform->nid,
                'new_uri' => $site->title,
                'new_db_server' => $site->db_server,
              ),
            );
          }
        }

        break;

      }
    }

    if (empty($source_platform) || empty($destination_platform)) {
      drupal_set_message(t("Couldn't find both matching platforms for server !server_nid (!server_name)",
        array('!server_nid' => $server_nid, '!server_name' => $servers[$server_nid]->title)));
    }
    else if ($source_platform == $destination_platform) {
      // This could lead to huge issues. Better stop the press.
      drupal_set_message(t("Error: Both the source and destination platforms are the same on !server_nid (!server_name)",
        array('!server_nid' => $server_nid, '!server_name' => $servers[$server_nid]->title)), 'error');

      return array();
    }
  }

  return $tasks;
}

/**
 * Finds which platforms to run the mass tasks on for a specific server
 *
 * @param $server_nids
 *   Array of server nids, representing all the servers we want to run these on
 * @param $task_type string
 * @param $site_pattern string
 *   regex to restrict which sites to run the task on. Compares to site->title
 * @return An associative array of tasks that must be run per server (indexed by
 *   server nid). Includes a human-readable array and the computed values array.
 */
function hosting_network_compute_basic_tasks($server_nids, $task_type, $site_pattern = '') {
  $servers = node_load_multiple(array_values($_GET['servers']));
  $tasks = array();

  foreach ($server_nids as $server_nid) {
     $sites = (array) hosting_network_rest_call($server_nid, 'hosting_site');

    // Note: For some reason the array isn't associative.
    foreach ($sites as $site) {
      if (empty($site_pattern) || preg_match($site_pattern, $site->title)) {
        $tasks[$server_nid][] = array(
          'task_type' => $task_type,
          'site_name' => $site->title,
          'server_nid' => $server_nid,
          'target_nid' => $site->nid,
        );
      }
    }
  }

  return $tasks;
}
