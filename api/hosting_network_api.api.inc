<?php

/**
 * Returns a site and the server it's on (or NULL if none found)
 */
function hosting_network_api_get_site_by_url($site_url, $hostname = NULL) {
  $network_nodes = hosting_get_servers('network_node');

  foreach($network_nodes as $server_nid => $network_hostname) {
    $sites = (array) hosting_network_rest_call($server_nid, 'hosting_site');

    // TODO: Replace with the retrieve (by url) REST call once it works
    foreach ($sites as $site) {
      if ($site->title == $site_url && (empty($hostname) || $network_hostname == $hostname)) {
        $output = array(
          'server' => $server_nid,
          'site' => hosting_network_rest_call($server_nid, 'hosting_site/get_by_nid', array('nid' => $site->nid), 'POST'),
        );

        return $output;
      }
    }
  }
}

/**
 * Returns a list of sites
 *
 * Grouped by server nid
 */
function hosting_network_api_list_sites($hostname, $format) {
  $format = strtolower($format);

  $sites = array();
  if (empty($hostname)) {
    $servers = hosting_get_servers('network_node');

    foreach ($servers as $server_nid => $server) {
      $sites[$server_nid] = (array) hosting_network_rest_call($server_nid, 'hosting_site');
    }
  }
  else {
    $server = hosting_network_api_get_server_by_hostname($hostname);
    $sites[$server->nid] = (array) hosting_network_rest_call($server->nid, 'hosting_site');
  }

  switch ($format) {
    case 'plaintext':
      $plaintext = '';
      foreach ($sites as $server_nid => $server_sites) {
        $indentation = "";
        if (empty($hostname)) {
          // Only specify the hostnames if we weren't given one
          $server_node = node_load($server_nid);
          $plaintext .= $server_node->title . "\n";
          $indentation = "  ";
        }

        foreach ($server_sites as $site_nid => $site) {
          $plaintext .= $indentation . $site->title . "\n";
        }
      }

      return $plaintext;
      break;
    case 'json':
      // Need a separate array because we can't edit an array while iterating it
      $json_sites = array();

      foreach ($sites as $server_nid => $server_sites) {
        $server = node_load($server_nid);

        foreach ($server_sites as $site) {
          $json_sites[$server->title][] = $site->title;
        }
      }

      if (!empty($hostname)) {
        // We don't need the hostname if we already have it
        $json_sites = array_pop($json_sites);
      }

      return json_encode($json_sites, JSON_PRETTY_PRINT);
      break;
  }

  return $sites;
}

/**
 * Returns a list of network nodes
 */
function hosting_network_api_list_servers() {
  $servers = hosting_get_servers('network_node');

  return node_load_multiple(array_keys($servers));
}

/**
 * Returns a server node (or NULL if none is found)
 */
function hosting_network_api_get_server_by_hostname($hostname) {
//  $nodes = node_load_multiple(NULL, array("title" => $hostname));
  $servers = hosting_get_servers('network_node');

  foreach ($servers as $nid => $server_hostname) {
    if ($server_hostname == $hostname) {
      return node_load($nid);
    }
  }
}
