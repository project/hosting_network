<?php

/**
 * Callback for the drush-demo-command command
 */
function drush_hosting_network_import_servers($file) {
  hosting_network_import_servers_from_file($file);
}
