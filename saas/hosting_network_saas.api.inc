<?php

/**
 * @file
 * This file defines the functions used in the Services resources defined in the
 * hosting_network_saas.module.
 */

/**
 * Returns a list of network nodes
 */
function hosting_network_saas_list_servers($saas_distro = '') {
  $servers = hosting_network_saas_get_servers($saas_distro);

  return node_load_multiple(array_keys($servers));
}

/**
 * Finds and returns the SaaS server with the least sites on it.
 */
function hosting_network_saas_select_server_for_site_creation($saas_distro = '') {
  $servers = hosting_network_saas_get_servers($saas_distro);

  $min_sites = PHP_INT_MAX;
  foreach ($servers as $server_nid => $server) {
    $sites = (array) hosting_network_rest_call($server_nid, 'hosting_site');

    if (count($sites) < $min_sites) {
      $min_server = array($server_nid => $server);
    }
  }

  return $min_server;
}

/**
 * Dispatches a request for a new site.
 *
 * If hostname is empty, the function will select the emptiest available server.
 *
 * @param $new_site_fqdn
 *   The fully qualified domain name of the site we're about to create.
 * @param $hostname
 *   The server to which to dispatch the request. If empty, will be selected
 *   automatically from the pool of available servers.
 * @param $options
 *   Additional options if any. Can be used with hosting_saas_utils to set
 *   variables in the site at creation time. The invoice ID is mandatory
 *   but will be generated automatically from a timestamp if empty.
 * @param $saas_distro
 *   If hostname is empty, we can restrict the selection of servers to those
 *   which serve this distro. This is only useful if you've set a distribution
 *   in the server settings.
 * @return Nid of the new clone task if successful, nothing otherwise.
 * @see hosting_network_saas_select_server_for_site_creation()
 */
function hosting_network_saas_dispatch_saas_request($new_site_fqdn, $hostname = '', $options = array(), $saas_distro = '') {
  module_load_include('api.inc', 'hosting_network_api');

  if (empty($hostname)) {
    $server = hosting_network_saas_select_server_for_site_creation($saas_distro);
    $array = array_pop($server);
    $hostname = $array['title'];
  }

  $server = hosting_network_api_get_server_by_hostname($hostname);

  if (empty($server)) {
    throw new Exception(t('Error: Server not found with hostname @hostname.', array('@hostname' => $hostname)));
  }

  // invoice ID acts as a transaction ID, so we can use a timestamp if needed.
  if (empty($post_options['invoice'])) {
    $post_options['invoice'] = REQUEST_TIME;
  }

  if (isset($server->services['network_node']->saas_api_key)) {
    $post_options['api-key'] = $server->services['network_node']->saas_api_key;
  }

  $post_options['nid'] = '';
  $post_options['type'] = 'clone';
  $post_options['options'] = $options;
  $post_options['options']['new_uri'] = $new_site_fqdn;

  $endpoint = 'http://' . $hostname . '/aegir/saas/task';

  // Do the curl thing

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $endpoint);
  curl_setopt($ch, CURLOPT_POST, 1);

  // Must specify '&' because default is dumb
  $build_query = http_build_query($post_options, NULL, '&');

  curl_setopt($ch, CURLOPT_POSTFIELDS, $build_query);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

  $raw_so = curl_exec($ch);

  // Simplest way to get an array
  $xml = simplexml_load_string($raw_so);
  $json = json_encode($xml);
  $server_output = json_decode($json, TRUE);

  curl_close ($ch);

  // Detect if there's an error
  if (empty($server_output)) {
    throw new Exception(t('There was an error with the submission.'));
  }
  else {
    // It worked
    return $server_output['nid'];
  }
}
