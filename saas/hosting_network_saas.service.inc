<?php

/**
 * @file
 * This file is to define a Hosting service, NOT a Services resources.
 */

class hostingService_network_node_hostmaster_saas extends hostingService_network_node_hostmaster {
  public $type = 'hostmaster_saas';
  public $has_port = FALSE;

  function form(&$form) {
    parent::form($form);
    $form['saas_distro'] = array(
      '#type' => 'textfield',
      '#required' => FALSE,
      '#title' => t('SaaS distribution'),
      '#default_value' => isset($this->saas_distro) ? $this->saas_distro : NULL,
      '#description' => t("The SaaS distribution this server can host."),
      '#size' => 30,
      '#weight' => 20,
    );

    $form['saas_api_key'] = array(
      '#type' => 'textfield',
      '#required' => FALSE,
      '#title' => t('SaaS API key'),
      '#default_value' => isset($this->saas_api_key) ? $this->saas_api_key : NULL,
      '#description' => t("Set this if you've set an API key in your server's SaaS settings."),
      '#size' => 30,
      '#weight' => 25,
    );
  }

  function insert() {
    parent::insert();
    db_update('hosting_network_server')
      ->fields(array(
        'saas_distro' => $this->saas_distro,
        'saas_api_key' => $this->saas_api_key,
      ))
      ->condition('nid', $this->server->nid, '=')
      ->condition('vid', $this->server->vid, '=')
      ->execute();
  }

  function update() {
    parent::update();
    db_update('hosting_network_server')
      ->fields(array(
        'saas_distro' => $this->saas_distro,
        'saas_api_key' => $this->saas_api_key,
      ))
      ->condition('nid', $this->server->nid, '=')
      ->condition('vid', $this->server->vid, '=')
      ->execute();
  }

  function delete_revision() {
    parent::delete_revision();
  }

  function delete() {
    parent::delete();
  }

  function load() {
    parent::load();
    $this->mergeData('SELECT oauth_key, oauth_secret, saas_distro, saas_api_key FROM {hosting_network_server} WHERE vid = :vid', array(':vid' => $this->server->vid));
  }

  function view(&$render) {
    parent::view($render);

    $render['saas_distro'] = array(
      '#type' => 'item',
      '#title' => t('SaaS Distribution'),
      '#markup' => filter_xss($this->saas_distro),
    );
  }

  public function context_options($task_type, $ref_type, &$task) {
    parent::context_options($task_type, $ref_type, $task);
    $task->context_options[$this->service . '_saas_distro'] = $this->saas_distro;
    $task->context_options[$this->service . '_saas_api_key'] = $this->saas_api_key;
  }

  public function context_import($context) {
    parent::context_import($context);

    $this->saas_distro = $context->saas_distro;
    $this->saas_api_key = $context->saas_api_key;
  }


}

