<?php
/**
 * @file
 * Code for the Hosting Network node feature.
 *
 * Mainly creates a default oauth consumer and adds a drush function for it.
 */

include_once 'hosting_network_node.features.inc';

define('HOSTING_NETWORK_NODE_DEFAULT_CONSUMER_NAME', 'hosting_network_node_default_consumer');

/**
 * Implements hook_drush_command().
 */
function hosting_network_node_drush_command() {
  $items['hosting-network-node-get-default-oauth-consumer'] = array(
    'description' => 'Returns the default oauth consumer, formatted in JSON.',
    'aliases' => array('hnn-default'),
    'arguments' => array(
      'parameter' => 'Get a specific parameter of the consumer (eg: "key")',
    ),
  );

  return $items;
}

/**
 * Returns our default admin consumer. Creates it if it's missing.
 */
function hosting_network_node_get_default_admin_consumer() {
  $query = db_select('oauth_common_provider_consumer', 'ocpc')
    ->fields('ocpc', array('csid'))
    ->condition('ocpc.name', HOSTING_NETWORK_NODE_DEFAULT_CONSUMER_NAME)
    ->range(0, 1)
    ->execute();
  $result = $query->fetchObject();

  if (!empty($result)) {
    return oauth_common_consumer_load($result->csid);
  }
  else {
    $consumer = new DrupalOAuthConsumer(user_password(32), user_password(32), array(
      'name' => HOSTING_NETWORK_NODE_DEFAULT_CONSUMER_NAME,
      'context' => 'hosting_network_node_context',
      'uid' => 1,
      'provider_consumer' => TRUE,
    ));

    $consumer->write();

    return $consumer;
  }
}

/**
 * Removes our default admin consumer. Mainly used for uninstalling.
 *
 * @return TRUE if the consumer was deleted, FALSE otherwise.
 */
function hosting_network_node_delete_default_admin_consumer() {
  $total = db_delete('oauth_common_provider_consumer')
  ->condition('name', HOSTING_NETWORK_NODE_DEFAULT_CONSUMER_NAME)
  ->execute();

  return ($total > 0);
}
