<?php

/**
 * Callback for the drush-demo-command command
 */
function drush_hosting_network_node_get_default_oauth_consumer($parameter = NULL) {
  $consumer = hosting_network_node_get_default_admin_consumer();
  if (empty($parameter)) {
    return json_encode($consumer);
  }
  else {
    return $consumer->$parameter;
  }
}
